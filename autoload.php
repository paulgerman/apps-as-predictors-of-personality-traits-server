<?php
include_once("functions".DIRECTORY_SEPARATOR."functions.php");
spl_autoload_register(
	function($strClassName)
	{
		$strFileName = __DIR__.DIRECTORY_SEPARATOR.str_replace("\\", DIRECTORY_SEPARATOR, $strClassName).".php";
		if(!file_exists($strFileName))
			throw new GeneralException("File does not exist ". $strFileName);
		require_once(__DIR__.DIRECTORY_SEPARATOR.str_replace("\\", DIRECTORY_SEPARATOR, $strClassName).".php");
	}
);