<?php
namespace Objects;


class App extends BaseObject
{

	public static $strObjectName = "app";
	public static $strIndexProp = "app_id";
	public static $strTableName = "apps";

	public static $arrProps = array(
		"app_id",
		"app_name",
		"app_category",
		"app_count",
		"app_rating_E",
		"app_rating_A",
		"app_rating_C",
		"app_rating_N",
		"app_rating_O",
	);

	public static $arrFictionalProps = array(

	);

	public static $arrPropsEnum = array(

	);

	public static $arrPropsOptional = array(
		"app_category" => "",
		"app_count" => 1,
	);

	public static $arrPropsReadOnly = array(

	);

	public static $arrRenderProps = array(
	);

	public $arrValues = array();

}