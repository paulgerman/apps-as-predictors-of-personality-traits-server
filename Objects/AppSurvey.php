<?php
namespace Objects;


class AppSurvey extends BaseObject
{

	public static $strObjectName = "appSurveys";
	public static $strIndexProp = "app_survey_id";
	public static $strTableName = "app_surveys";

	public static $arrProps = array(
		"app_survey_id",
		"app_survey_content",
		"app_survey_time",
		"app_survey_results",
		"app_survey_type",
		"app_survey_complete",
		"app_survey_code",
		"app_survey_processed",
		"app_survey_results_AI",
		"app_master_version",
		"app_survey_IP"
	);

	public static $arrFictionalProps = array(
	);

	public static $arrPropsEnum = array(
		"app_survey_type" => array(
			"learning",
			"AI"
		),
		"app_survey_complete" => array(
			"yes",
			"no"
		)
	);

	public static $arrPropsOptional = array(
		"app_survey_results" => "",
		"app_survey_complete" => "no",
		"app_survey_processed" => "no",
		"app_survey_results_AI" => ""
	);

	public static $arrPropsReadOnly = array(
		"app_survey_time",
		"app_survey_code",
		"app_survey_complete",
		"app_survey_results_AI",
		"app_survey_results",
		"app_survey_IP"
	);

	public static $arrRenderProps = array(
	);

	public $arrValues = array();

}