<?php
namespace Objects
{
	abstract class BaseObject
	{
		//usually the table name without 's' at the end
		public static $strObjectName;

		//index column name
		public static $strIndexProp;

		public static $strTableName;

		//all properties of the object that are in db
		public static $arrProps = array();

		//all properties of the object that are not in db and are added by code
		public static $arrFictionalProps = array();
		
		//properties that have fixed values from a list
		public static $arrPropsEnum = array();

		//properties that are optional and have a default value
		public static $arrPropsOptional = array();

		//properties that are readonly and can't be updated/inserted
		public static $arrPropsReadOnly = array();

		/*
		 * render properties when generating forms
		 * example: 
		 * array("product_id" => array(
		 *                  "displayName" => "ID",
		 *                  "class" => "text-left custom-class"
		 * ))
		*/
		public static $arrRenderProps = array();

		

		//associative array with the values
		public $arrValues = array();

		function __construct($arrValues)
		{
			if(static::$strTableName == "")
				static::$strTableName = static::$strObjectName."s";

			if(!is_array($arrValues))
			{
				throw new \GeneralException("param arrValues needs to be array!");
			}
			$this->arrValues = $arrValues;
			$this->validate();
		}

		public function validate()
		{
			$this->add_optional_props();
			$this->check_enums();

			$bValid = true;
			$arrMissingProps = array();
			//verify that all needed props exist
			foreach(static::$arrProps as $strColumnName)
			{
				if(!array_key_exists($strColumnName, $this->arrValues) && $strColumnName != static::$strIndexProp && !in_array($strColumnName, static::$arrPropsReadOnly))
				{
					$arrMissingProps[] = $strColumnName;
					$bValid = false;
				}
			}

			if(!$bValid)
			{
				throw new \GeneralException("Properties: ".implode(", ", $arrMissingProps)." are missing");
			}

			//remove all not needed props
			foreach($this->arrValues as $strPropName => $strValue)
			{
				if(!in_array($strPropName, static::$arrProps))
				{
					unset($this->arrValues[$strPropName]);
				}
			}
		}

		/**
		 * Build the insert query part. Ex: (`column1`, `column2`) VALUES ('a1', 'a2')
		 *
		 * @return string
		 */
		public function build_insert_query()
		{
			$pdo = trdb();
			$strColumns = "(";
			$strData = "(";
			foreach($this->arrValues as $strColumnName => $strPropData)
			{
				if($strColumnName != static::$strIndexProp)
				{
					$strColumns .= "`".$strColumnName."`, ";
					$strData .= $pdo->quote($strPropData).", ";
				}
			}
			$strColumns = substr($strColumns, 0, -2).")";
			$strData = substr($strData, 0, -2).")";
			$strQuery = $strColumns."\nVALUES ".$strData;

			return $strQuery;
		}

		public function add_optional_props()
		{
			foreach(static::$arrPropsOptional as $strPropName => $strDefaultValue)
			{
				if(!array_key_exists($strPropName, $this->arrValues))
				{
					$this->arrValues[$strPropName] = $strDefaultValue;
				}
			}
		}

		public function check_enums()
		{
			foreach(static::$arrPropsEnum as $strPropName => $arrAllowedValues)
			{
				if(!in_array($this->arrValues[$strPropName], $arrAllowedValues))
				{
					throw new \GeneralException("Property: ".$strPropName." has value ".$this->arrValues[$strPropName]." and must have one of these values: ".implode(", ", $arrAllowedValues));
				}
			}
		}

		public static function remove_read_only_props(&$arrValues)
		{
			foreach(static::$arrPropsReadOnly as $strPropName)
			{
				if(array_key_exists($strPropName, $arrValues))
				{
					unset($arrValues[$strPropName]);
				}
			}

		}
	}
}