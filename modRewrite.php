<?php
error_reporting(E_ALL &~ E_STRICT);
assert_options(ASSERT_BAIL, 1);
ini_set("display_errors", 1);
ini_set("error_log", "php-error.log");


$arrAllowedPages = array(
	"publicEndpoint" => array(
		"autoload.php",
		"config.php",
		"classes/JSONRPC/Endpoints/PublicEndpoint.php",
	),
	"" => array("demo/quiz.php"),
);

$arrAllowedFolders = array(
);

$strRequest = $_GET["request"];
unset($_GET["request"]);


if(array_key_exists($strRequest, $arrAllowedPages))
{
	foreach($arrAllowedPages[$strRequest] as $strInclude)
	{

		if(file_exists($strInclude))
		{
			require_once($strInclude);
		}
		else
		{
			throw new GeneralException("File ".$strInclude." not found!");
		}
	}
}
else
{
	foreach($arrAllowedFolders as $strFolder)
	{
		if(strpos($strRequest, $strFolder) === 0)
		{
			if(is_file($strRequest))
			{
				require_once($strRequest);
			}
			else
			{
				if($strRequest != $strFolder && $strRequest != $strFolder."/")
					continue;

				$strRequest .= DIRECTORY_SEPARATOR;
				if(file_exists($strRequest."index.html"))
					require_once($strRequest."index.html");
				else if(file_exists($strRequest."index.php"))
					require_once($strRequest."index.php");
				else
				{
					header("HTTP/1.0 404 Not Found");
					die("no index page in folder ".$strFolder);
				}
			}
			exit();
		}
	}
	header("HTTP/1.0 404 Not Found");
	die("Page requested does not exist!");
}
