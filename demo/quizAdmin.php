<a href="?learn=1">Learn!</a><br>
<a href="?AI=1">Process!</a><br>
<?php
require_once(realpath(dirname(dirname(__FILE__)))."/autoload.php");
require_once(realpath(dirname(dirname(__FILE__)))."/config.php");

echo require_once_res(__DIR__, "css/bootstrap.min.css");

$appController = new \Controllers\AppController();
$surveyController = new \Controllers\AppSurveyController();


if(isset($_GET["learn"]))
{
	$appController->delete_all();

	$arrSurveys = $surveyController->get_all(0, 99999, "WHERE `app_survey_type` = 'learning' AND `app_survey_complete` = 'yes'");

	foreach($arrSurveys as $arrSurvey)
	{
		$appController->processSurveyLearning($arrSurvey);
	}

	echo "Done";
}

if(isset($_GET["AI"]))
{
	$arrSurveys = $surveyController->get_all(0, 99999, "WHERE `app_survey_type` = 'AI'");

	foreach($arrSurveys as $arrSurvey)
	{
		$appController->processSurveyAI($arrSurvey);
	}

	echo "Done";
}

?>

