<head>
	<?php
	require_once(realpath(dirname(dirname(__FILE__)))."/autoload.php");
	require_once(realpath(dirname(dirname(__FILE__)))."/config.php");

	echo require_once_res(__DIR__, "css/bootstrap.min.css");
	?>
	<style>
		input[type="radio"] {
			width:20px;
			height: 20px;
			margin-left: -25px !important;
			margin-top: 8px !important;
		}
		input[type="submit"] {
			width: 200px;
			height: 50px;
			font-size: 27px;
		}
	</style>
	<meta name="viewport" content="width=device-width, initial-scale=0.6">
</head>
<body>

<div class="container-fluid">

<?php
	$pdo = trdb();
	$ok = false;
	$bAlreadyCompleted = false;
	if(isset($_GET["code"]))
	{
		$_GET["code"] = strtoupper($_GET["code"]);
		if(isset($_GET["reset"]))
		{
			$strQuery = "UPDATE `app_surveys` SET `app_survey_complete` = 'no' WHERE `app_survey_code` = ".$pdo->quote($_GET["code"]);
			$pdo->exec($strQuery);
		}
		$strQuery = "SELECT `app_survey_complete`, `app_survey_results`, `app_survey_type`, `app_survey_results_AI`, `app_survey_processed` from `app_surveys` WHERE `app_survey_code` = ".$pdo->quote($_GET["code"]);
		$arrResults = trdb()->query($strQuery)->fetchAll(\PDO::FETCH_ASSOC);
		if(count($arrResults) > 0)
		{
			$ok = true;
			if(
				$arrResults[0]["app_survey_type"] == "learning" && $arrResults[0]["app_survey_complete"] == "yes"
				|| $arrResults[0]["app_survey_type"] == "AI" && $arrResults[0]["app_survey_processed"] == "yes"
			)
			{
				$bAlreadyCompleted = true;
			}
		}
	}

	$appController = new \Controllers\AppController();


	if($bAlreadyCompleted)
	{
		if($arrResults[0]["app_survey_type"] == "AI")
		{
			$bAI = true;
			echo '<div class="alert alert-success" style="font-size: 30px;">Here are your computed results :D &lt;3</div>';
		}
		else
		{
			$bAI = false;
			echo '<div class="alert alert-success" style="font-size: 30px;">Your survey was already completed! Thank you!</div>';
			$appSurveyController = new \Controllers\AppSurveyController();
			echo '<div class="alert alert-success" style="font-size: 30px;">If computed by the system your results would have an error of '.
				round($appSurveyController->calculateErrorPercentageSurvey($_GET['code']), 1)
				.'%!</div>';
		}

		if(!$bAI)
			displayResults((array)json_decode($arrResults[0]["app_survey_results"]), $appController->calculateResultsAI($appSurveyController->get($_GET["code"], "app_survey_code")));
		else
			displayResults((array)json_decode($arrResults[0]["app_survey_results_AI"]));

		if(!$bAI)
		{
			echo '<a href="?code='.$_GET["code"].'&reset=1';
			if(isset($_GET['m']))
				echo '&m=1';
			echo '">';
			if(isset($_GET["m"]))
				echo '<input type="submit" class="btn btn-primary" style="width:100%;" value="Reset Quiz"></span></a><br><br>';
			else
				echo '<input type="submit" class="btn btn-primary" value="Reset Quiz"></span></a><br><br>';

		}

		if(isset($_GET["m"]))
			echo '<input type="submit" value="Exit" class="btn btn-danger" style="width:100%;" onclick="Android.close();">';
	}
	else if($ok)
	{
		$arrQuestions = array(
			"Am the life of the party",
			"Feel little concern for others",
			"I am always prepared.",
			"I get stressed out easily.	",
			"I have a rich vocabulary.	",
			"I don't talk a lot.",
			"I am interested in people.",
			"I leave my belongings around.",
			"I am relaxed most of the time.",
			"I have difficulty understanding abstract ideas.",
			"I feel comfortable around people.",
			"I insult people.",
			"I pay attention to details.",
			"I worry about things.",
			"I have a intense imagination.",
			"I keep in the background. (not draw attention / just blend in and go with the flow)",
			"I sympathize with others' feelings.",
			"I make a mess of things.",
			"I rarely feel sad.",
			"I am not interested in abstract ideas.",
			"I start conversations.",
			"I am not interested in other people's problems.",
			"I get chores done right away.",
			"I am easily disturbed.",
			"I have excellent ideas.",
			"I have little to say.",
			"I have a soft heart.",
			"I often forget to put things back in their proper place.",
			"I get upset easily.",
			"I do not have a good imagination.",
			"I talk to a lot of different people at parties.",
			"I am not really interested in others.",
			"I like order.",
			"I change my mood a lot.",
			"I am quick to understand things.",
			"I don't like to draw attention to myself.",
			"I take time out for others.",
			"I avoid doing my duties that I'm supposed to do.",
			"I have frequent mood swings.",
			"I use difficult words.",
			"I don't mind being the center of attention.",
			"I feel others' emotions.",
			"I follow a schedule.",
			"I get irritated easily.",
			"I spend time reflecting on things.",
			"I am quiet around strangers.",
			"I make people feel comfortable/relaxed/secure.",
			"I am exacting in my work.",
			"I often feel sad or depressed.",
			"I am full of ideas."
		);

		$bFormDisplay = true;

		if(!empty($_POST))
		{
			$bOK = true;
			for($i = 1; $i <= 50; $i++)
			{
				if(!isset($_POST["q".$i]))
				{
					echo '<div class="alert alert-danger">You haven\'t answered all questions!</div>';
					$bOK = false;
					break;
				}
			}

			$q = $_POST;
			$arrResults = array();

			if($bOK)
			{
				$arrResults["E"] = 20;
				$arrResults["A"] = 14;
				$arrResults["C"] = 14;
				$arrResults["N"] = 38;
				$arrResults["O"] = 8;

				$sE = array(1,-1,1,-1,1,-1,1,-1,1,-1);
				$sA = array(-1,1,-1,1,-1,1,-1,1,1,1);
				$sC = array(1,-1,1,-1,1,-1,1,-1,1,1);
				$sN = array(-1,1,-1,1,-1,-1,-1,-1,-1,-1);
				$sO = array(1,-1,1,-1,1,-1,1,1,1,1);

				for($i = 1; $i <= 46; $i+=5)
				{
					$arrResults["E"] += $sE[intval(($i-1) / 5)] * $q["q".$i];
					$arrResults["A"] += $sA[intval(($i-1) / 5)] * $q["q".($i+1)];
					$arrResults["C"] += $sC[intval(($i-1) / 5)] * $q["q".($i+2)];
					$arrResults["N"] += $sN[intval(($i-1) / 5)] * $q["q".($i+3)];
					$arrResults["O"] += $sO[intval(($i-1) / 5)] * $q["q".($i+4)];
				}
				$strResults = json_encode($arrResults);
				$strQuery = "UPDATE `app_surveys` SET `app_survey_results` = ".$pdo->quote($strResults)." WHERE `app_survey_code` = ".$pdo->quote($_GET["code"]);
				$appSurveyController = new \Controllers\AppSurveyController();
				trdb()->exec($strQuery);

				echo '<div class="alert alert-success" style="font-size: 30px;">Thank you for your time!</div>';
				echo '<div class="alert alert-success" style="font-size: 30px;">If computed by the system your results would have an error of '.
					round($appSurveyController->calculateErrorPercentageSurvey($_GET['code']), 1)
					.'%!</div>';


				displayResults($arrResults, $appController->calculateResultsAI($appSurveyController->get($_GET["code"], "app_survey_code")));
				if(isset($_GET["m"]))
					echo '<br><br><input type="submit" value="Exit" style="width: 100%;" class="btn btn-danger" onclick="Android.close();">';

				$strQuery = "UPDATE `app_surveys` SET `app_survey_complete` = 'yes' WHERE `app_survey_code` = ".$pdo->quote($_GET["code"]);
				trdb()->exec($strQuery);

				$bFormDisplay = false;
			}
		}

		if($bFormDisplay)
		{
			?>
			<div style="font-size: 36px; font-weight: bold;" onclick="rand()">Quiz</div>
			<form method="post" name="form1" action="?code=<?=$_GET["code"]?><?php
if(isset($_GET['m']))
	echo '&m=1';
?>">
				<table class="table table-hover table-striped text-center" style="font-size:29px;">
					<thead>
					<tr>
						<th>Question</th>
						<th>
							<small>Disagree</small>
						</th>
						<th></th>
						<th>
							<small>Neutral</small>
						</th>
						<th></th>
						<th>
							<small>Agree</small>
						</th>
					</tr>
					</thead>
					<tbody>

					<?php
					foreach($arrQuestions as $nKey => $strQuestion)
					{
						echo '<tr><td class="text-left">'.($nKey + 1).". ".$strQuestion."</td>";
						for($i = 1; $i <= 5; $i++)
						{
							echo '<td><label class="radio-inline"><input required value="'.$i.'" type="radio" name="q'.($nKey + 1).'">'.$i.'</label></td>';
						}
						echo "</tr>";
					}
					?>
					<tr>
						<td colspan="6"><input name="submit" type="submit" class="btn btn-primary"></td>
					</tr>
					</tbody>
				</table>
			</form>


			<?php
		}
	}
	else
	{
		if(isset($_GET['code']))
		{
			echo '<div class="alert alert-danger">Invalid code!</div>';
		}
		?>
		<form method="get">
			<label>Code to begin: <input type="text" name="code" class="form-control"></label>
			<input type="submit" class="btn btn-primary">
		</form>
	<?php
	}
	?>
</div>

<script>
	var miau = 0;
	function rand()
	{
		miau++;
		if(miau < 3)
			return;
		var radios = {};
		var radionames = [];

		var tmpname;
		for (var i = 0; i < document.form1.elements.length; i++)
		{
			if (document.form1.elements[i].type == "radio")
			{
				tmpname = document.form1.elements[i].name;
				if (!radios[tmpname])
				{
					radios[tmpname] = [];
					radionames.push(tmpname);
				}
				radios[tmpname].push(i);
			}
		}

		var idx;
		for (var i = 0; i < radionames.length; i++)
		{
			tmpname = radionames[i];
			idx = Math.floor(Math.random() * (radios[tmpname].length - 1));
			document.form1.elements[radios[tmpname][idx]].checked = true;
		}
	}
</script>

<?php

function displayResults($arrResults, $arrComputed = array())
{
	?>
	<div class="alert alert-info" style="font-size: 30px;">
		<h3>Scores are between 0 and 40</h3>
		<p>Extroversion: <strong><?=$arrResults["E"]?></strong>
			<?php
			if(!empty($arrComputed))
				echo "(".$arrComputed["E"]." - system calculated)";
			?></p>
		<p>Agreeableness: <strong><?=$arrResults["A"]?></strong>
			<?php
			if(!empty($arrComputed))
				echo "(".$arrComputed["A"]." - system calculated)";
			?></p>
		<p>Conscientiousness: <strong><?=$arrResults["C"]?></strong>
			<?php
			if(!empty($arrComputed))
				echo "(".$arrComputed["C"]." - system calculated)";
			?></p>
		<p>Neuroticism: <strong><?=$arrResults["N"]?></strong>
			<?php
			if(!empty($arrComputed))
				echo "(".$arrComputed["N"]." - system calculated)";
			?></p>
		<p>Openness to Experience: <strong><?=$arrResults["O"]?></strong>
			<?php
			if(!empty($arrComputed))
				echo "(".$arrComputed["O"]." - system calculated)";
			?></p>
		<br>
		<?php
		if(!empty($arrComputed))
			echo "*<strong>Bold</string> numbers are the actual quiz results";
		?>
		<br>
		<br>
		<p><ul>
			<li>Extroversion (E) is the personality trait of seeking fulfillment from sources outside the self or
		in community. High scorers tend to be very social while low scorers prefer to work on their
		projects alone.</li><br>
			<li> Agreeableness (A) reflects much individuals adjust their behavior to suit others. High scorers
		are typically polite and like people. Low scorers tend to 'tell it like it is'.</li><br>
			<li> Conscientiousness (C) is the personality trait of being honest and hardworking. High scorers
		tend to follow rules and prefer clean homes. Low scorers may be messy and cheat others.</li><br>
			<li> Neuroticism (N) is the personality trait of being emotional.</li><br>
			<li> Openness to Experience (O) is the personality trait of seeking new experience and intellectual
		pursuits. High scores may day dream a lot. Low scorers may be very down to earth.</li><br>
		</ul>
		</p>
	</div>

	<?php
}
?>
</body>
