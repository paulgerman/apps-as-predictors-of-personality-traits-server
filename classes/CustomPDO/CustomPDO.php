<?php
namespace classes\CustomPDO;
include "CustomPDOStatement.php";
class CustomPDO extends \PDO
{
	protected static $singleton = null;
	protected static $arrConfig = null;

	public $nQueryNum = 0;

	public static function setConfig($arrConfig)
	{
		static::$arrConfig = $arrConfig;
	}

	public function __construct($arrConfig)
	{
		// turn of error reporting
		mysqli_report(MYSQLI_REPORT_OFF);

		// connect to database
		parent::__construct(
			"mysql:host=".$arrConfig["strHost"]."; port=3306; dbname=".$arrConfig["strDBName"].";",
			$arrConfig["strDBUsername"],
			$arrConfig["strDBPassword"]
		);
		$this->setAttribute(\PDO::ATTR_STATEMENT_CLASS, array('CustomPDOStatement', array($this)));


		// check if a connection established
		if(mysqli_connect_errno())
		{
			throw new \Exception(mysqli_connect_error(), mysqli_connect_errno());
		}
	}

	public static function singleton()
	{
		if(!self::$singleton)
		{
			self::$singleton = new self(static::$arrConfig);
		}
		return self::$singleton;
	}

	public function query($strQuery)
	{
		$statement = parent::query($strQuery);
		if($this->errorCode() != '00000')
		{
			$arrErrorInfo = $this->errorInfo();
			throw new \GeneralException($arrErrorInfo[0].": ".$arrErrorInfo[2]." ".$strQuery);
		}
		$this->nQueryNum++;
		return $statement;
	}

	public function exec($strQuery)
	{
		$nAffectedRows = parent::exec($strQuery);
		if($this->errorCode() != '00000')
		{
			$arrErrorInfo = $this->errorInfo();
			throw new \GeneralException($arrErrorInfo[0].": ".$arrErrorInfo[2]." ".$strQuery);
		}
		$this->nQueryNum++;
		return $nAffectedRows;
	}
}