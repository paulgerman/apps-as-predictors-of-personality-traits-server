<?php

namespace classes\JSONRPC\Endpoints;
use classes\JSONRPC\JSONRPCException;
use classes\JSONRPC\Plugins\FilterPlugin;
use classes\JSONRPC\Server;

class PublicEndpoint extends Server
{
	function allowedFunctions()
	{
		return array(
			"survey_create" => "\\Controllers\\AppSurveyController()->create",
			"survey_process" => "\\Controllers\\AppSurveyController()->processAI",
			"survey_error" => "\\Controllers\\AppSurveyController()->calculateErrorPercentageSurvey",
			"global_error" => "\\Controllers\\AppSurveyController()->calculateErrorPercentage",
		);
	}
}
if(count($_POST))
{
	if(isset($_POST["q"]))
	{
		$strJSONEncodedData = $_POST["q"];
	}
	else
	{
		exit("Nothing requested :( q parameter is missing");
	}
}
else if(count($_GET))
{
	if(isset($_GET["q"]))
	{
		$strJSONEncodedData = $_GET["q"];
	}
	else
	{
		exit("Nothing requested :( q parameter is missing");
	}
}
else
{
	?>
	<form method="post">
		<textarea name="q" style="width: 100%;height: 50%">{"method":"method_name","params":[]}</textarea>
		<input type="hidden" name="pretty" value="1">
		<input type="submit" value="SUBMIT">
	</form>
	<?php
	exit("Nothing requested :(");
}
error_log(gmdate("Y-m-d\\TH:i:s\\Z")." - ".$_SERVER["REMOTE_ADDR"].": ".$strJSONEncodedData."\n\n",3,"jsonRequests.log");

$arrData = json_decode($strJSONEncodedData, true);
try
{
	if (json_last_error() !== JSON_ERROR_NONE)
	{
		throw new JSONRPCException("Invalid json");
	}
	$arrData = (array)$arrData;

	$jsonRPC = new PublicEndpoint();

	error_log(gmdate("Y-m-d\\TH:i:s\\Z")." - ".$_SERVER["REMOTE_ADDR"].": ".var_export($arrData,true)."\n\n",3,"requests.log");

	if(array_key_exists("pretty", $_REQUEST))
		echo '<textarea style="width: 100%; height: 100%;">'.json_encode(json_decode($jsonRPC->processRequestBatch($arrData)),JSON_PRETTY_PRINT).'</textarea>';
	else
		echo $jsonRPC->processRequestBatch($arrData);
}
catch(JSONRPCException $exc)
{
	echo json_encode(array(
		"jsonrpc" => "2.0",
		"error" => array(
			"code" => $exc->getCode(),
			"message" => $exc->getMessage(),
		),
	));
}
