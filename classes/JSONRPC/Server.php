<?php

namespace classes\JSONRPC;


use classes\JSONRPC\Plugins\PluginBase;

abstract class Server
{
	/**
	 * @var Plugins\PluginBase[]
	 */
	protected $plugins = array();

	protected $arrAllowedFunctions = array();

	/**
	 * Return an associative array of strings that represent the allowed function calls.
	 * To allow a class with all functions: "user" => "\\Controllers\\UserController",
	 * To allow only certain functions from a class: "user_get" => "\\Controllers\\UserController()->get"
	 * To allow a function: "destroy_everything_alive" => "destroy_everything_alive"
	 * @return array
	 */
	abstract function allowedFunctions();

	public function __construct()
	{
		$this->arrAllowedFunctions = $this->allowedFunctions();
	}

	public function addPlugin(PluginBase $plugin)
	{
		$this->plugins[] = $plugin;
	}

	final function processRequestBatch($arrData)
	{
		if(is_array($arrData))
			if(is_associative_array($arrData))
				return $this->processRequest($arrData);
			else
			{
				$arrAnswers = [];
				foreach($arrData as $arrRequest)
					$arrAnswers[] = $this->processRequest($arrRequest);
				return '['.join(",", $arrAnswers)."]";
			}
		else
			throw new JSONRPCException("Data must be array");
	}

	final function processRequest($arrData)
	{
		try
		{
			$pdo = trdb();
		}
		catch(\PDOException $exc)
		{
			throw new JSONRPCException($exc->getMessage());
		}

		if(!is_array($arrData))
		{
			throw new JSONRPCException("Data must be array");
		}
		if(!array_key_exists("method", $arrData))
		{
			throw new JSONRPCException("'method' key does not exist in array");
		}
		if(!array_key_exists("params", $arrData))
		{
			throw new JSONRPCException("'params' key does not exist in array");
		}

		if(is_object($arrData["params"]))
		{
			$arrData["params"] = (array)$arrData["params"];
		}

		if(!is_array($arrData["params"]))
		{
			throw new JSONRPCException("'params' key must be an array");
		}

		foreach($this->plugins as $plugin)
		{
			$arrData = $plugin->beforeProcess($arrData);
		}

		if(array_key_exists($arrData["method"], $this->arrAllowedFunctions))
		{
			if($this->arrAllowedFunctions[$arrData["method"]] == $arrData["method"])
			{
				if(function_exists($arrData["method"]))
				{
					$reflectionMethod = new \ReflectionFunction($arrData["method"]);
					$this->_checkParams($arrData["params"], $reflectionMethod);

					try
					{
						$pdo->beginTransaction();
						$arrResult = call_user_func_array($arrData["method"], $arrData["params"]);
						$pdo->commit();
					}
					catch(\GeneralException $exc)
					{
						return $this->_logError($exc);
					}
				}
				else
				{
					throw new JSONRPCException("Function ".$arrData["method"]." is not defined");
				}
			}
			else
			{
				$strCallInfo = $this->arrAllowedFunctions[$arrData["method"]];
				$strClassName = substr($strCallInfo, 0, strpos($strCallInfo, "()->"));
				if(empty($strClassName))
				{
					throw new JSONRPCException("Method from class ".$arrData["method"]." not specified");
				}
				$strFunctionName = substr($strCallInfo, strpos($strCallInfo, "()->") + 4);
				try
				{
					$arrResult = $this->_callClassMethod($strClassName, $strFunctionName, $arrData["params"]);
				}
				catch(\GeneralException $exc)
				{
					return $this->_logError($exc);
				}
			}
		}
		else if(array_key_exists(substr($arrData["method"], 0, strpos($arrData["method"], "_")), $this->arrAllowedFunctions))
		{
			$strClassName = $this->arrAllowedFunctions[substr($arrData["method"], 0, strpos($arrData["method"], "_"))];
			$strFunctionName = substr($arrData["method"], strpos($arrData["method"], "_") + 1);
			try
			{
				$arrResult = $this->_callClassMethod($strClassName, $strFunctionName, $arrData["params"]);
			}
			catch(\GeneralException $exc)
			{
				return $this->_logError($exc);
			}
		}
		else
		{
			throw new JSONRPCException("Function ".$arrData["method"]." is not allowed on this endpoint!");
		}

		foreach($this->plugins as $plugin)
		{
			$arrResult = $plugin->afterProcess($arrResult);
		}

		$strResult =  json_encode(array(
			"jsonrpc" => "2.0",
			"result" => $arrResult,
		));
		return $strResult;
	}

	private function _callClassMethod($strClassName, $strFunctionName, $arrParams)
	{
		$pdo=trdb();
		if(class_exists($strClassName))
		{
			$reflection = new \ReflectionClass($strClassName);
			$arrMethods = $reflection->getMethods(\ReflectionMethod::IS_PUBLIC);
			foreach($arrMethods as $method)
			{
				if($method->getName() == $strFunctionName)
				{
					$objController = new $strClassName();

					$reflectionMethod = new \ReflectionMethod($strClassName, $strFunctionName);
					$this->_checkParams($arrParams, $reflectionMethod);

					$pdo->beginTransaction();
					$arrResult = call_user_func_array(array(&$objController, $strFunctionName), $arrParams);
					$pdo->commit();
					return $arrResult;

				}
			}
			throw new JSONRPCException("Method ".$strFunctionName." does not exist in class ".$strClassName);
		}
		else
		{
			throw new JSONRPCException("Class ".$strClassName." does not exist!");
		}
	}

	private function _checkParams($arrParams, \ReflectionFunctionAbstract $reflectionFunction)
	{
		if($this->_arrayIsAssoc($arrParams))
		{
			//TODO
			throw new JSONRPCException("TODO!!!");
		}
		else
		{
			if(count($arrParams) < $reflectionFunction->getNumberOfRequiredParameters() || count($arrParams) > $reflectionFunction->getNumberOfParameters())
			{
				$arrRequiredParams = array();
				$arrOptionalParams = array();

				$arrReflectionParams = $reflectionFunction->getParameters();
				foreach($arrReflectionParams as $reflectionParameter)
				{
					if(!$reflectionParameter->isOptional())
					{
						$arrRequiredParams[] = $reflectionParameter->getName();
					}
					else
					{
						$arrOptionalParams[] = $reflectionParameter->getName()." = ".$reflectionParameter->getDefaultValue();
					}
				}
				if(count($arrParams) < $reflectionFunction->getNumberOfRequiredParameters())
				{
					throw new JSONRPCException("Too few parameters. Required params: ".implode(", ", $arrRequiredParams)." Optional params: ".implode(", ", $arrOptionalParams));
				}
				else if(count($arrParams) > $reflectionFunction->getNumberOfParameters())
				{
					throw new JSONRPCException("Too many parameters. Required params: ".implode(", ", $arrRequiredParams)." Optional params: ".implode(", ", $arrOptionalParams));
				}
			}
		}
	}

	private function _arrayIsAssoc(array $array) {
		return (bool)count(array_filter(array_keys($array), 'is_string'));
	}

	private function _logError(\Exception $exc)
	{
		$pdo=trdb();
		if($pdo->inTransaction())
			$pdo->rollBack();
		error_log(gmdate("Y-m-d\\TH:i:s\\Z")." - ".$_SERVER["REMOTE_ADDR"].": ".$exc->getFile()."#".$exc->getLine()."\n".$exc->getMessage()."\n".$exc->getTraceAsString()."\n\n",3,"errors.log");
		return json_encode(array(
			"jsonrpc" => "2.0",
			"error" => array(
				"code" => $exc->getCode(),
				"message" => $exc->getMessage(),
			),
		));
	}
}