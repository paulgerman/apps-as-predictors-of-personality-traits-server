-- Adminer 4.2.4 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `apps`;
CREATE TABLE `apps` (
  `app_id` int(11) NOT NULL AUTO_INCREMENT,
  `app_name` varchar(255) NOT NULL,
  `app_count` int(11) NOT NULL,
  `app_rating_E` double NOT NULL,
  `app_rating_A` double NOT NULL,
  `app_rating_C` double NOT NULL,
  `app_rating_N` double NOT NULL,
  `app_rating_O` double NOT NULL,
  `app_category` varchar(255) NOT NULL,
  PRIMARY KEY (`app_id`),
  KEY `app_name` (`app_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `app_surveys`;
CREATE TABLE `app_surveys` (
  `app_survey_id` int(11) NOT NULL AUTO_INCREMENT,
  `app_survey_content` text NOT NULL,
  `app_survey_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `app_survey_results` text NOT NULL,
  `app_survey_results_AI` varchar(255) NOT NULL,
  `app_survey_type` enum('learning','AI') NOT NULL,
  `app_survey_complete` enum('yes','no') NOT NULL DEFAULT 'no',
  `app_survey_code` varchar(100) NOT NULL,
  `app_survey_processed` enum('yes','no') NOT NULL,
  `app_master_version` int(11) NOT NULL,
  `app_survey_IP` varchar(255) NOT NULL,
  PRIMARY KEY (`app_survey_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


-- 2016-09-13 19:44:32
