Presentation:

https://www.dropbox.com/s/tuj70sf0ln0m3qf/Presentation%20-%20Apps%20as%20Predictors%20of%20Personality%20Traits.pptx?dl=0

Paper:

https://www.dropbox.com/s/yohkeg368g0cyzs/Paper%20-%20Apps%20as%20Predictors%20of%20Personality%20Traits.pdf?dl=0