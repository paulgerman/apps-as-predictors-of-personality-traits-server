<?php
/**
 * @return classes\CustomPDO\CustomPDO
 */
function trdb()
{
	return classes\CustomPDO\CustomPDO::singleton();
}

//http://stackoverflow.com/questions/13076480/php-get-actual-maximum-upload-size
function convertPHPSizeToBytes($sSize)
{
	if ( is_numeric( $sSize) )
	{
		return $sSize;
	}
	$sSuffix = substr($sSize, -1);
	$iValue = substr($sSize, 0, -1);
	switch(strtoupper($sSuffix))
	{
		case 'P':
			$iValue *= 1024;
		case 'T':
			$iValue *= 1024;
		case 'G':
			$iValue *= 1024;
		case 'M':
			$iValue *= 1024;
		case 'K':
			$iValue *= 1024;
			break;
	}
	return $iValue;
}

function getMaximumFileUploadSize()
{
	return min(convertPHPSizeToBytes(ini_get('post_max_size')), convertPHPSizeToBytes(ini_get('upload_max_filesize')));
}

function require_once_res($strDirectory, $strFileRelative, $strAlternativeID = "")
{
	static $arrFilesAlreadyIncluded = array();
	$strRelativePath = str_replace(realpath($_SERVER['DOCUMENT_ROOT']), "", realpath($strDirectory));
	$strRelativePath = str_replace("\\", "/", $strRelativePath)."/";
	
	if(!in_array($strRelativePath.$strFileRelative, $arrFilesAlreadyIncluded) && !in_array($strAlternativeID, $arrFilesAlreadyIncluded))
	{
		$arrFilesAlreadyIncluded[] = $strRelativePath.$strFileRelative;
		if($strAlternativeID != "")
			$arrFilesAlreadyIncluded[] = $strAlternativeID;

		if(strtolower(pathinfo($strFileRelative, PATHINFO_EXTENSION)) == "js")
			return '<script src="'.$strRelativePath.$strFileRelative.'"></script>';
		else
			return '<link href="'.$strRelativePath.$strFileRelative.'" rel="stylesheet">';
	}
}



function browser_print($mxValue)
{
	if(is_array(($mxValue)))
		echo "<script>console.log('".javascript_escape(print_r($mxValue, true))."')</script>";
	else
		echo "<script>console.log('".javascript_escape($mxValue)."')</script>";
}

function javascript_escape($str) {
	$new_str = '';

	$str_len = strlen($str);
	for($i = 0; $i < $str_len; $i++) {
		$new_str .= '\\x' . sprintf('%02x', ord(substr($str, $i, 1)));
	}

	return $new_str;
}

function trimHTML($code, $limit = 300)
{
	if ( strlen($code) <= $limit )
	{
		return $code;
	}

	$html = substr($code, 0, $limit);
	preg_match_all ( "#<([a-zA-Z]+)#", $html, $result );

	foreach($result[1] AS $key => $value)
	{
		if ( strtolower($value) == 'br' )
		{
			unset($result[1][$key]);
		}
	}
	$openedtags = $result[1];

	preg_match_all ( "#</([a-zA-Z]+)>#iU", $html, $result );
	$closedtags = $result[1];

	foreach($closedtags AS $key => $value)
	{
		if ( ($k = array_search($value, $openedtags)) === FALSE )
		{
			continue;
		}
		else
		{
			unset($openedtags[$k]);
		}
	}

	if ( empty($openedtags) )
	{
		if ( strpos($code, ' ', $limit) == $limit )
		{
			return $html."...";
		}
		else
		{
			return substr($code, 0, strpos($code, ' ', $limit))."...";
		}
	}

	$position = 0;
	$close_tag = '';
	foreach($openedtags AS $key => $value)
	{
		$p = strpos($code, ('</'.$value.'>'), $limit);

		if ( $p === FALSE )
		{
			$code .= ('</'.$value.'>');
		}
		else if ( $p > $position )
		{
			$close_tag = '</'.$value.'>';
			$position = $p;
		}
	}

	if ( $position == 0 )
	{
		return $code;
	}

	return substr($code, 0, $position).$close_tag."...";
}

function displayQueryCount($strTag)
{
	echo '<script>console.log("#'.$strTag.' queries: '.trdb()->nQueryNum.'");</script>';
}

function is_associative_array($arr)
{
	if(!is_array($arr))
		return FALSE;
	
	foreach (array_keys($arr) as $key => $val)
	{
		if ($key !== $val)
		{
			return TRUE;
		}
	}

	return FALSE;
}

function retrieveObjConfig($strObjectName)
{
	$strObjectName = "\\Objects\\".$strObjectName;
	if(!class_exists($strObjectName))
		throw new GeneralException("Class ". $strObjectName ." does not exist");

	$arrProps = array(
		"cols" => $strObjectName::$arrRenderProps,
		"enumKeys" => $strObjectName::$arrPropsEnum,
		"indexKey" => $strObjectName::$strIndexProp,
	);
	return $arrProps;
}