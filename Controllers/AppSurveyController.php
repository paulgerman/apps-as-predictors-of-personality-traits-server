<?php
namespace Controllers
{
	class AppSurveyController extends BaseController
	{
		function __construct()
		{
			$this->strClassName = "\\Objects\\AppSurvey";
		}

		function edit($strID, $arrData)
		{
			$arrSurvey = parent::edit($strID, $arrData);
			$this->learnFromAll();
			return $arrSurvey;
		}

		function create($arrData)
		{
			$strClassName = $this->strClassName;
			$strClassName::remove_read_only_props($arrData);
			while(1)
			{
				$arrData["app_survey_code"] = $this->generateRandomString(6);
				try
				{
					$this->get($arrData["app_survey_code"], "app_survey_code");
				}
				catch(\GeneralException $exc)
				{
					if($exc->getCode() == \GeneralException::PRODUCT_NOT_FOUND)
						break;
				}
			}

			$arrData["app_survey_IP"] = md5($_SERVER['REMOTE_ADDR']);
			$arrSurvey = parent::createInternal($arrData);

			if($arrSurvey["app_survey_type"] == "AI")
			{
				$this->processAI($arrData["app_survey_code"]);
			}
			return $arrData["app_survey_code"];
		}

		private function generateRandomString($length)
		{
			return substr(str_shuffle("123456789ABCDEFGHIJKLMNPQRSTUVWXYZ"), 0, $length);
		}

		private function learnFromAll()
		{
			$appController = new AppController();
			$appController->delete_all();

			$arrSurveys = $this->get_all(0, 99999, "WHERE `app_survey_type` = 'learning' AND `app_survey_complete` = 'yes'");

			foreach($arrSurveys as $arrSurveyTmp)
			{
				$appController->processSurveyLearning($arrSurveyTmp);
			}
		}

		public function processAI($strCode)
		{
			$arrSurvey = $this->get($strCode, "app_survey_code");
			$arrSurvey["app_survey_processed"] = "no";
			$this->edit($arrSurvey["app_survey_id"], $arrSurvey);
			$appController = new AppController();
			$this->learnFromAll();

			$appController->processSurveyAI($arrSurvey);
			return $this->get($arrSurvey["app_survey_id"]);
		}

		public function calculateErrorPercentage()
		{
			$arrSurveys = $this->get_all(0, 99999, "WHERE `app_survey_type` = 'learning' AND `app_survey_complete` = 'yes'");
			$nSumAll = 0;
			foreach($arrSurveys as $arrSurvey)
			{
				$nSumAll += $this->calculateErrorPercentageSurveyInternal($arrSurvey);
			}
			return $nSumAll/count($arrSurveys);
		}

		public function calculateErrorPercentageSurvey($strCode)
		{
			return $this->calculateErrorPercentageSurveyInternal($this->get($strCode, "app_survey_code"));
		}

		public function calculateErrorPercentageSurveyInternal($arrSurvey)
		{
			$appController = new AppController();
			$arrResults = (array)json_decode($arrSurvey["app_survey_results"]);
			$arrComputedResults = $appController->calculateResultsAI($arrSurvey);
			$nSum = 0;
			foreach($arrResults as $key => $value)
			{
				$nSum += abs($value - $arrComputedResults[$key])/$value*100;

			}
			$nError = $nSum / count($arrResults);
			return $nError;
		}
	}
}