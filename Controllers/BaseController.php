<?php
namespace Controllers
{
	abstract class BaseController
	{
		public $strClassName;

		abstract function __construct();

		function create($arrData)
		{
			$strClassName = $this->strClassName;
			$strClassName::remove_read_only_props($arrData);
			return $this->createInternal($arrData);
		}

		function createInternal($arrData)
		{
			$strClassName = $this->strClassName;
			$newObject = new $strClassName($arrData);
			trdb()->exec(
				"INSERT INTO `".$strClassName::$strTableName."`
					".$newObject->build_insert_query()
			);

			$newObject->arrValues[$strClassName::$strIndexProp] = trdb()->lastInsertId();
			return $newObject->arrValues;
		}
		function delete($strID)
		{
			$strClassName = $this->strClassName;
			$nRowsAffected = trdb()->exec("DELETE FROM `".$strClassName::$strTableName."` WHERE `".$strClassName::$strIndexProp."` = ".(int)$strID);

			if($nRowsAffected != 1)
			{
				throw new \GeneralException($strClassName." with id ".(int)$strID." not found!", \GeneralException::PRODUCT_NOT_FOUND);
			}
			return true;
		}

		function delete_all($strCondition = "")
		{
			$strClassName = $this->strClassName;
			trdb()->exec("DELETE FROM `".$strClassName::$strTableName."` ".$strCondition);
		}

		function get($strValue, $strColumn = "")
		{
			$strClassName = $this->strClassName;
			if($strColumn == "")
				$strColumn = $strClassName::$strIndexProp;
			$object = null;
			$result = trdb()->query("SELECT * FROM `".$strClassName::$strTableName."` WHERE `".$strColumn."` = ".trdb()->quote($strValue));
			while($row = $result->fetchAll(\PDO::FETCH_ASSOC))
			{
				$object = new $strClassName($row[0]);
			}
			if($object == null)
			{
				throw new \GeneralException($strClassName." with ".$strColumn." = ".$strValue." not found!", \GeneralException::PRODUCT_NOT_FOUND);
			}
			return $object->arrValues;
		}

		function edit($strID, $arrData)
		{
			$strClassName = $this->strClassName;
			$strClassName::remove_read_only_props($arrData);
			return $this->editInternal($strID, $arrData);
		}

		function editInternal($strID, $arrData)
		{
			$pdo = trdb();
			$strClassName = $this->strClassName;

			$strQueryPart = "";
			foreach($strClassName::$arrProps as $strProp)
			{
				if(array_key_exists($strProp, $arrData) && $strProp != $strClassName::$strIndexProp)
				{
					$strQueryPart .= "`$strProp` = ".$pdo->quote($arrData[$strProp]).", ";
				}
			}
			$strQueryPart = substr($strQueryPart, 0, -2);
			if($strQueryPart != "")
			{
				trdb()->exec("
					UPDATE `".$strClassName::$strTableName."` SET
					".$strQueryPart."
					WHERE `".$strClassName::$strIndexProp."` = ".$pdo->quote($strID)."
				");
				return $this->get($strID);
			}
			else
			{
				throw new \GeneralException("No properties given to update");
			}
		}

		function get_all($nStart = 0, $nLimit = 10, $strCondition = "", $strSortCol = "", $strOrder = "ASC")
		{
			$strClassName = $this->strClassName;
			$arrObjects = array();
			$strQuery = "
				SELECT * FROM
				 `".$strClassName::$strTableName."`
				 ".$strCondition." ";
			if($strSortCol != "")
			{
				$strOrder = strtoupper($strOrder);
				if($strOrder != "ASC" && $strOrder != "DESC")
					throw new \GeneralException("Don't do this sir :(");

				$strQuery .= "ORDER BY `".$strSortCol."` ".$strOrder." ";
			}
			$strQuery .= "LIMIT ".(int)$nStart.", ".(int)$nLimit;
			$arrResults = trdb()->query($strQuery)->fetchAll(\PDO::FETCH_ASSOC);
			foreach($arrResults as $arrResult)
			{
				$object = new $strClassName($arrResult);
				$arrObjects[$object->arrValues[$strClassName::$strIndexProp]] = $object->arrValues;
			}
			return $arrObjects;
		}

		function processFile(&$arrData, $strKey, $strType)
		{
			$strClassName = $this->strClassName;
			$bOK = false;
			if(array_key_exists($strKey, $arrData))
			{
				$bUnset = false;
				if(is_array($arrData[$strKey]))
				{
					$strFileType = strtolower(pathinfo($arrData[$strKey]["name"], PATHINFO_EXTENSION));
					if(is_array($arrData[$strKey]) && $arrData[$strKey]["error"] != 4)
					{
						if($arrData[$strKey]["error"] == 0)
						{
							if(!getimagesize($arrData[$strKey]["tmp_name"]))
								throw new \GeneralException("Bad image");
							$strFileName = pathinfo($arrData[$strKey]["name"], PATHINFO_FILENAME);
						}
						else
						{
							throw new \GeneralException("Upload error: ".$arrData[$strKey]["error"]);
						}
					}
					else
						$bUnset = true;
				}
				else
				{
					if($arrData[$strKey] === "")
					{
						$arrData[$strKey] = "";
						return;
					}

					$nPosStart = strpos($arrData[$strKey], ":");
					$nPosEnd = strpos($arrData[$strKey], ";");
					if($nPosStart === false || $nPosEnd === false)
						$bUnset = true;

					$strFileType = substr($arrData[$strKey], $nPosStart + 1, $nPosEnd - $nPosStart - 1);

					$arrExtensions = array(
						'image/jpeg' => "jpeg",
						'image/png' => "png",
						'image/jpg' => "jpg",
						'image/gif' => "gif"
					);
					if(array_key_exists($strFileType, $arrExtensions))
						$strFileType = $arrExtensions[$strFileType];

					$strFileName = "json";
				}

				if($bUnset)
				{
					unset($arrData[$strKey]);

					if(array_key_exists($strKey."_delete", $arrData))
					{
						$arrData[$strKey] = "";
					}
					return;
				}

				if($strType == "image")
				{
					$arrAcceptedFormats = array(
						'gif', 'png', 'jpg', 'jpeg'
					);
					$strDirectoryRelativeToRoot = "uploadedImages";

				}
				elseif($strType == "file")
				{
					$arrAcceptedFormats = array();
					$strDirectoryRelativeToRoot = "uploadedFiles";
				}
				else
				{
					throw new \GeneralException("Invalid TYPE");
				}

				if(!in_array($strFileType, $arrAcceptedFormats) && !empty($arrAcceptedFormats))
				{
					throw new \GeneralException("Invalid file type ".$strFileType);
				}


				if($strType == "image")
				{
					if(isset($strClassName::$arrRenderProps[$strKey]) && isset($strClassName::$arrRenderProps[$strKey]["maxWidthOrHeight"]))
						$maxDim = $strClassName::$arrRenderProps[$strKey]["maxWidthOrHeight"];
					else
						$maxDim = 3086;


					if(is_array($arrData[$strKey]))
					{
						list($nWidth, $nHeight) = getimagesize($arrData[$strKey]["tmp_name"]);
						$nWidthOrg = $nWidth;
						$nHeightOrg = $nWidth;
						$blobImg = file_get_contents($arrData[$strKey]["tmp_name"]);
						$img = imagecreatefromstring($blobImg);

					}
					else
					{
						$blobImg = base64_decode(substr($arrData[$strKey], strpos($arrData[$strKey], ",") + 1));
						$img = imagecreatefromstring($blobImg);
						$nWidthOrg = $nWidth = imagesx($img);
						$nHeightOrg = $nHeight = imagesy($img);

					}



					if($nWidth > $maxDim || $nHeight > $maxDim)
					{
						$ratio = $nWidth / $nHeight; // width/height
						if($ratio > 1)
						{
							$nWidth = $maxDim;
							$nHeight = $maxDim / $ratio;
						}
						else
						{
							$nWidth = $maxDim * $ratio;
							$nHeight = $maxDim;
						}
						$dst = imagecreatetruecolor($nWidth, $nHeight);

						imagealphablending($dst, false);
						imagesavealpha($dst, true);

						imagecopyresampled($dst, $img, 0, 0, 0, 0, $nWidth, $nHeight, $nWidthOrg, $nHeightOrg);
						imagedestroy($img);

						ob_start();
						if($strFileType == "png")
							imagepng($dst);
						elseif($strFileType == "jpg" || $strFileType == "jpeg")
							imagejpeg($dst);
						elseif($strFileType == "gif")
							imagegif($dst);
						imagedestroy($dst);

						$blobImg = ob_get_contents();
						ob_end_clean();
					}
				}

				//$target_file = $target_dir.basename($arrData["image"]["name"]);
				$strRelativePath = "/".$strDirectoryRelativeToRoot."/".uniqid($strFileName."-").".".$strFileType;
				$strTargetFile = $_SERVER["DOCUMENT_ROOT"].$strRelativePath;
				$arrData[$strKey] = $strRelativePath;


				if($strType == "image")
				{
					if(!file_put_contents($strTargetFile, $blobImg))
						throw new \GeneralException("Can't upload file!");
				}
				else
				{
					if(is_array($arrData[$strKey]))
						if(!move_uploaded_file($arrData[$strKey]["tmp_name"], $strTargetFile))
						{
							throw new \GeneralException("Can't upload file");
						}
					else
					{
						file_put_contents($strTargetFile, base64_decode(substr($arrData[$strKey], strpos($arrData[$strKey], ",") + 1)));
					}
				}
				$bOK = true;
			}
		}
	}
}