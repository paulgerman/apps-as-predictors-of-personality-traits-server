<?php
namespace Controllers
{

	use Objects\AppSurvey;

	class AppController extends BaseController
	{
		function __construct()
		{
			$this->strClassName = "\\Objects\\App";
		}

		function create($arrData)
		{
			$strClassName = $this->strClassName;
			$object = parent::create($arrData);


			return $object;
		}

		function processSurveyLearning(Array $arrSurvey)
		{
			$arrPersonalities = array("E", "A", "C", "N", "O");
			if($arrSurvey["app_survey_complete"] != "yes")
				throw new \GeneralException("Survey is not complete!");

			if($arrSurvey["app_survey_processed"] == "yes")
				throw new \GeneralException("Survey has been already processed!");

			if($arrSurvey["app_survey_type"] != "learning")
				throw new \GeneralException("Survey must be type learning!");

			$arrApps = (array)json_decode($arrSurvey["app_survey_content"]);
			$arrResults = (array)json_decode($arrSurvey["app_survey_results"]);

			$arrResultsPercent = array();
			foreach($arrPersonalities as $strPersonality)
			{
				$arrResultsPercent[$strPersonality] = $arrResults[$strPersonality] / 40;
			}

			foreach($arrApps as $strAppName)
			{
				try
				{
					$arrApp = $this->get($strAppName, "app_name");

					$arrApp["app_count"]++;

					foreach($arrPersonalities as $strPersonality)
					{
						$arrApp["app_rating_".$strPersonality] = ($arrApp["app_count"] * $arrApp["app_rating_".$strPersonality] + $arrResultsPercent[$strPersonality]) / ($arrApp["app_count"] + 1);
					}

					$this->edit($arrApp["app_id"], $arrApp);
				}
				catch(\GeneralException $exc)
				{
					if($exc->getCode() != \GeneralException::PRODUCT_NOT_FOUND)
						throw $exc;

					$arrObject = array(
						"app_name" => $strAppName,
						"app_category" => "",
					);

					foreach($arrPersonalities as $strPersonality)
					{
						$arrObject["app_rating_".$strPersonality] = $arrResultsPercent[$strPersonality];
					}

					$this->create($arrObject);
				}
			}

			$surveyController = new AppSurveyController();
			$arrSurvey["app_survey_processed"] = "yes";
			//$surveyController->edit($arrSurvey["app_survey_id"], $arrSurvey);
		}

		function processSurveyAI(Array $arrSurvey)
		{

			if($arrSurvey["app_survey_processed"] == "yes")
				throw new \GeneralException("Survey has been already processed!");

			if($arrSurvey["app_survey_type"] != "AI")
				throw new \GeneralException("Survey must be type AI!");
			$surveyController = new AppSurveyController();


			$arrSurvey["app_survey_processed"] = "yes";
			$arrSurvey["app_survey_results_AI"] = json_encode($this->calculateResultsAI($arrSurvey));
			$surveyController->editInternal($arrSurvey["app_survey_id"], $arrSurvey);
		}

		function calculateResultsAI(Array $arrSurvey)
		{
			$arrPersonalities = array("E", "A", "C", "N", "O");

			$arrApps = (array)json_decode($arrSurvey["app_survey_content"]);

			$arrResults = array_combine($arrPersonalities, array(0,0,0,0,0));
			$nAppsFound = 0;
			foreach($arrApps as $strApp)
			{
				try
				{
					$arrApp = $this->get($strApp, "app_name");
					foreach($arrPersonalities as $strPersonality)
					{
						$arrResults[$strPersonality] += $arrApp["app_rating_".$strPersonality];
					}
					$nAppsFound++;
				}
				catch(\GeneralException $exc)
				{
					if($exc->getCode() != \GeneralException::PRODUCT_NOT_FOUND)
						throw $exc;
				}
			}

			if($nAppsFound != 0)
				foreach($arrPersonalities as $strPersonality)
				{
					$arrResults[$strPersonality] /= $nAppsFound;
				}

			$arrSurvey["app_survey_processed"] = "yes";
			foreach($arrResults as &$value)
			{
				$value = round($value * 40);
			}
			return $arrResults;
		}
	}
}